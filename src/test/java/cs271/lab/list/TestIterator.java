package cs271.lab.list;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class
TestIterator {

  private List<Integer> list;
  // See the Java List Interface documentation to understand what all the List methods do ...

  @Before
  public void setUp() throws Exception {
    list = new ArrayList<Integer>();
    // TODO also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
  }

  @After
  public void tearDown() throws Exception {
    list = null;
  }

  @Test
  public void testEmpty() {
    final Iterator<Integer> i = list.iterator();
    assertFalse(i.hasNext());   // This test is Okey. There is no element so the the assertFalse goes Okey.
  }

  @Test
  public void testNonempty() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    final Iterator<Integer> i = list.iterator();
    assertTrue(i.hasNext());  // Needs Confirmation;
    assertEquals(33, i.next().intValue());
    // TODO fix the expected values in the assertions below
    assertTrue(i.hasNext());
    assertEquals(77, i.next().intValue());
    assertTrue(i.hasNext());
    assertEquals(44, i.next().intValue());
    assertTrue(i.hasNext());
    assertEquals(77, i.next().intValue());
    assertTrue(i.hasNext());
    assertEquals(55, i.next().intValue());
    assertTrue(i.hasNext());
    assertEquals(77, i.next().intValue());
    assertTrue(i.hasNext());
    assertEquals(66, i.next().intValue());
    assertFalse(i.hasNext());
  }

  @Test
  public void testRemove() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    final Iterator<Integer> i = list.iterator();
    while (i.hasNext()) {
      if (i.next() == 77) {
        i.remove(); // TODO what happens if you use list.remove(Integer.valueOf(77))?
                    // The above code removes all the values with 77 in the list;
      }
    }
    // TODO using assertEquals and Arrays.asList, express which values are left in the list

      assertEquals(Arrays.asList(33, 44, 55, 66), list);

    // See TestList.java for examples of how to use Arrays.asList; also see the Java Arrays
    // class for more information
    //fail("Not yet implemented"); // remove this line when done
  }

  @Test
  public void testAverageValues() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    //double sum = 0;
    //int n = 0;
    // TODO use an iterator and a while loop to compute the average (mean) of the values
      int items = 0;
      int counter = 0;
      Iterator<Integer> i = list.iterator();
      while (i.hasNext()){
          items = items + i.next().intValue();
          counter ++;
      }

      double average = items / counter;
      assertEquals(61.0 , average, 0.1);  // Tests if the expected value(items/counter) is equall to the actual value (average);

    // (defined as the sum of the items divided by the number of items)
    // testNonempty shows how to use an iterator; use i.hasNext() in the while loop condition ???? I don't know what it means;

    //assertEquals(61.0, sum / n, 0.1);  // Tested with my own variables.
    //assertEquals(7, n);  //


      assertEquals(7, counter);


  }
}
