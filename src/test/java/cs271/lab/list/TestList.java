package cs271.lab.list;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TestList {

  private List<Integer> list;

  @Before
  public void setUp() throws Exception {
    list = new ArrayList<Integer>();
    // TODO also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
  }

  @After
  public void tearDown() throws Exception {
    list = null;
  }

  @Test
  public void testSizeEmpty() {
    assertTrue(list.isEmpty());
    assertEquals(0, list.size());
    try {
      list.get(0);
      fail("there should not be any items in the list");
    } catch (Exception ex) {
    }
  }

  @Test
  @DisplayName("TestSizeNonEmpty")
  public void testSizeNonEmpty() {
    // TODO fix the expected values in the assertions below
    list.add(77);
    System.out.println("Print This before the exacution");
    assertEquals(false, list.isEmpty());
    assertEquals(1, list.size());
    assertEquals(77, list.get(0).intValue());
  }

  @Test
  @DisplayName("TestConatins")
  public void testContains() {
    // TODO write assertions using

    assertFalse(list.contains(77));
    list.add(77);
    assertTrue(list.contains(77));
  }

  @Test
  @DisplayName("Show this")
  public void testAddMultiple() {
    list.add(77);
    list.add(77);
    list.add(77);
    // TODO fix the expected values in the assertions below
    assertEquals(3, list.size());
    assertEquals(0, list.indexOf(77));
    assertEquals(77, list.get(1).intValue());
    assertEquals(2, list.lastIndexOf(77));
  }

  @Test
  @DisplayName("TestAddMultiple2")
  public void testAddMultiple2() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    // TODO fix the expected values in the assertions below
    assertEquals(7, list.size());
    assertEquals(1, list.indexOf(77));
    assertEquals(5, list.lastIndexOf(77));
    assertEquals(44, list.get(2).intValue());
    assertEquals(77, list.get(3).intValue());
    assertEquals(Arrays.asList(33, 77, 44, 77, 55, 77, 66), list);
  }

  @Test
  @DisplayName("TestRemoveObject")
  public void testRemoveObject() {
    list.add(3);
    list.add(77);
    list.add(4);
    list.add(77);
    list.add(5);
    list.add(77);
    list.add(6);
    list.remove(5); // TODO answer: what does this method do?
    // Removes item from the list based on the index which will be defined inside the remove(parameter);

    // TODO fix the expected values in the assertions below
    assertEquals(6, list.size());
    assertEquals(1, list.indexOf(77));
    assertEquals(3, list.lastIndexOf(77));
    assertEquals(4, list.get(2).intValue());
    assertEquals(77, list.get(3).intValue());
    list.remove(Integer.valueOf(5));
    // TODO answer: what does this one do?
    // Removes items from the list based on the Integer Value provide by the valueOf() as parameter;


    assertEquals(5, list.size());
    assertEquals(1, list.indexOf(77));
    assertEquals(3, list.lastIndexOf(77));
    assertEquals(4, list.get(2).intValue());
    assertEquals(77, list.get(3).intValue());
  }

  //These Steps must be rechecked;

  @Test
  public void testContainsAll() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    // TODO using containsAll and Arrays.asList (see above),          // i Could not Catch What I should really do;

    // 1) assert that list contains all five different numbers added

    assertFalse(list.containsAll(Arrays.asList(33, 11, 22, 88, 55)));


    // 2) assert that list does not contain all of 11, 22, and 33
    assertTrue(list.containsAll(Arrays.asList(77, 66, 55, 44, 33)));

    //fail("Not yet implemented"); // remove this line when done
  }

  @Test
  public void testAddAll() {
    // TODO in a single statement using addAll and Arrays.asList,

    Collections.addAll(list, 33, 77, 44, 77, 55, 77, 66);

    assertEquals(Arrays.asList(33, 77, 44, 77, 55, 77, 66), list);

    // add items to the list to make the following assertions pass
    // (without touching the assertions themselves)
    //list.add
    assertEquals(7, list.size());
    assertEquals(33, list.get(0).intValue());
    assertEquals(77, list.get(1).intValue());
    assertEquals(44, list.get(2).intValue());
    assertEquals(77, list.get(3).intValue());
    assertEquals(55, list.get(4).intValue());
    assertEquals(77, list.get(5).intValue());
    assertEquals(66, list.get(6).intValue());
  }

  @Test
  public void testRemoveAll() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);

    // TODO in a single statement using removeAll and Arrays.asList,
    // remove items from the list to make the following assertions pass
    // (without touching the assertions themselves)

    assertTrue(list.removeAll(Arrays.asList(33, 44, 55, 66)));

    assertEquals(3, list.size());
    assertEquals(Arrays.asList(77, 77, 77), list);
  }

  @Test
  public void testRetainAll() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    // TODO in a single statement using retainAll and Arrays.asList,
    // remove items from the list to make the following assertions pass
    // (without touching the assertions themselves)

    assertEquals(7, list.size());

    Iterator<Integer> i = list.iterator();
    while(i.hasNext()){
      if(i.next().intValue() != 77){
        i.remove();
      }
    }
    assertEquals(3, list.size());
    assertEquals(Arrays.asList(77, 77, 77), list);  //needs to be rechecked;
  }
        // TODO : I should find out what the exact code is;
  @Test
  public void testSet() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    // TODO use the set method to change specific elements in the list
    // such that the following assertions pass
    // (without touching the assertions themselves)

    Iterator<Integer> i = list.iterator();
    int counter = 0;
    while(i.hasNext()){
      if(i.next() == 77){
        list.set(counter, 99);
      }
      counter++;
    }

    assertEquals(7, list.size());
    assertEquals(33, list.get(0).intValue());
    assertEquals(99, list.get(1).intValue());
    assertEquals(44, list.get(2).intValue());
    assertEquals(99, list.get(3).intValue());
    assertEquals(55, list.get(4).intValue());
    assertEquals(99, list.get(5).intValue());
    assertEquals(66, list.get(6).intValue());
  }

  @Test
  public void testSubList() {
    list.add(33);
    list.add(77);
    list.add(44);
    list.add(77);
    list.add(55);
    list.add(77);
    list.add(66);
    // TODO fix the arguments in the subList method so that the assertion
    // passes
    assertEquals(Arrays.asList(44, 77, 55), list.subList(2, 5));
  }
}
