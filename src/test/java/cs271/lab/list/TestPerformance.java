package cs271.lab.list;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestPerformance {

  // TODO run test and record running times for SIZE = 10, 100, 1000, 10000, ...
  // (choose in conjunction with REPS below up to an upper limit where the clock
  // running time for some methods is in the order of seconds, probably around 10_000)
  // TODO (optional) refactor to DRY
  // TODO answer: which of the two lists performs better for each method as the size increases?

  private final int SIZE = 10_000;

  // TODO choose this value in such a way that you can observe an actual effect
  // for increasing problem sizes - 1_000_000 is probably sufficient

  private final int REPS = 1_000_000;

  private List<Integer> arrayList;

  private List<Integer> linkedList;

  @Before
  public void setUp() {
    arrayList = new ArrayList<Integer>(SIZE);
    linkedList = new LinkedList<Integer>();
    for (int i = 0; i < SIZE; i++) {
      arrayList.add(i);
      linkedList.add(i);
    }
  }

  @After
  public void tearDown() {
    arrayList = null;
    linkedList = null;
  }


  // TODO : Number of times the Test will take time to exacute.
  // with 10 : 1st Try: 3m 0s, 2nd Try: 46s, 3rd Try: 32s, 4th Try: 1m 16s, 5th Try: 1m 20s;
  // with 100: 1st Try: 1m 7s, 2nd Try: 1m 18s, 3rd Try: 1m 26s;
  // with 1000: 1st Try: 19s, 2nd Try: 40s, 3rd Try: 49s;
  // with 10000: 1st Try: 24s, 2nd Try: 17s, 3rd Try: 15s, 4th Try: 20s, 5th Try: 35s;
  @Test
  public void testLinkedListAddRemove() {
    for (int r = 0; r < REPS; r++) {
      linkedList.add(0, 77);
      linkedList.remove(0);
    }
  }
  // TODO : Number of times the Test will take time to exacute.                               //Comparison
  // with 10 : 1st Try: 1m 50s, 2nd Try: 51s, 3rd Try: , 4th Try: 28s, 5th Try: 29s;          // with small size the ArrayListAddRemove is working more efficiently on my PC
  // with 100: 1st Try: 45s, 2nd Try: 3m 20s, 3rd Try: 1m 22s;
  // with 1000: 1st Try: 11s, 2nd Try: 45s, 3rd Try: 58s;
  // with 10000: 1st Try: 54s, 2nd Try: 30s, 3rd Try: 40s, 4th Try: 18s, 5th Try: 22s;        // meanwhile, as the size increases still the ArraylistAddRemove works more efficiently.
  @Test
  public void testArrayListAddRemove() {
    for (int r = 0; r < REPS; r++) {
      arrayList.add(0, 77);
      arrayList.remove(0);
    }
  }
  // TODO : Number of times the Test will take time to exacute.
  // with 10 : 1st Try: 1m 5s, 2nd Try: 17s, 3rd Try: , 4th Try: 9s, 5th Try: 5s;
  // with 100: 1st Try: 1m 14s, 2nd Try: 56s, 3rd Try: 1m 23s;
  // with 1000: 1st Try: 20s, 2nd Try: 23s, 3rd Try: 45s;
  // with 10000: 1st Try: 52s, 2nd Try: 57s, 3rd Try: 47s, 4th Try: 31s, 5th Try: 33s;
  @Test
  public void testLinkedListAccess() {
    long sum = 0;
    for (int r = 0; r < REPS; r++) {
      sum += linkedList.get(r % SIZE);
    }
  }
  // TODO : Number of times the Test will take time to exacute.                                 //Comparison
  // with 10 : 1st Try: 8s, 2nd Try: 15s, 3rd Try: 10s, 4th Try: 6s, 5th Try: 8s;               // with small size the ArrayListAccess is working more efficiently and in less time
  // with 100: 1st Try: 16s, 2nd Try: 10s, 3rd Try: 4s;
  // with 1000: 1st Try: 1m 55s, 2nd Try: 45s, 3rd Try: 1m 42s;
  // with 10000: 1st Try: 1m 38s, 2nd Try: 1m 24s, 3rd Try: 48s, 4th Try: 42s, 5th Try: 39s;    // but when the size increases the TestLinkedAccess works better and in less time.
  @Test
  public void testArrayListAccess() {
    long sum = 0;
    for (int r = 0; r < REPS; r++) {
      sum += arrayList.get(r % SIZE);
    }
  }
}
